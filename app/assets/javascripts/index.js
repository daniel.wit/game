var team = "";
var slider = new MasterSlider();
var slide_diogo = new Array(true,true,false,false,false);
var slide_jose = new Array(true,true,false,false,false);	

$(document).ready(function(){

	"use strict";



	$.getJSON( "/index/persons.json", function( data ) {

		$.each(data, function( index, obj ) {			
			if(obj.status == true) {				
				$('#' + obj.team + '-' + obj.name).css('display','block');
				$('#cross-' + obj.name).css('display','block');
			}
		});
	});
	

	$.getJSON( "/index/boards.json", function( data ) {
		$.each(data, function( index, obj ) {

			switch(obj.team) {
				case "diogo":
					slide_diogo[obj.board] = obj.status;
				break;
				case "jose":
					slide_jose[obj.board] = obj.status;
				break;
			}

		});
	});

    slider.setup('masterslider' , {
        width:1280,
        height:720,
        //space:100,
        fullwidth:false,
        centerControls:false,
        speed:150,
        view:'flow',
        grabCursor: false
    });
    //slider.control('arrows');
    slider.control('bullets' ,{autohide:false});   





    slider.api.addEventListener(MSSliderEvent.CHANGE_START , function(){    	
    	if (window.team == "" && slider.api.index() > 1) {
    		console.log('invalid team : ' + slider.api.index());
    		slider.api.previous();
    		alert(_invalid_sentences() + "\n\nCHOOSE YOUR TEAM!!!!!!!!!");    		
    	} else if(window.team == "diogo" && !slide_diogo[slider.api.index()]) {
    		console.log('invalid diogo');
    		slider.api.previous();
    		alert(_invalid_sentences() + "\n\nYou can't access to this board, yet. Figure it out!!!!!!!!!");    		    		
    	} else if(window.team == "jose" && !slide_jose[slider.api.index()]) {
    		console.log('invalid jose');
    		slider.api.previous();
    		alert(_invalid_sentences() + "\n\nYou can't access to this board, yet. Figure it out!!!!!!!!!");
    	}
	});

	


});


var process = function(name){
	console.log(name + ' ACTIVATED');

	switch(name) {
		case 'diogo':
			window.team = "diogo";
			$('#unlock-diogo').modal('show');
		break;
		case 'jose':
			window.team = "jose";
			$('#unlock-jose').modal('show');			
		break;
		default:
			belongs_to_team(window.team, name);			
		break;
	}

}


var unlock_board = function(team, board) {
	$.post( "/index/unlock_board", {team: team, board: board } );

	switch(team) {
		case 'diogo':
			slide_diogo[board] = true;
		break;
		case 'jose':
			slide_jose[board] = true;
		break;
	}

	$('.modal').modal('hide');	
	slider.api.next();

}


var unlock_person = function(name) {

	console.log($('#enigma_rogerio_answer').val());

	//VERIFY WRONG ANSWER
	switch(name) {
		case 'ana':
			if(parseInt($('#enigma_ana_answer').val())!=24) {
				alert(_invalid_sentences() + "\n\nNNNNNNOOOOOOOOOO!!!!!!!!!");
				return false;
			}
		break;
		case 'vera':
			if(parseInt($('#enigma_vera_answer').val())!=24) {
				alert(_invalid_sentences() + "\n\nNNNNNNOOOOOOOOOO!!!!!!!!!");
				return false;
			}
		break;
		case 'liliana':
			if(parseInt($('#enigma_liliana_answer').val())!=0) {
				alert(_invalid_sentences() + "\n\nNNNNNNOOOOOOOOOO!!!!!!!!!");
				return false;
			}
		break;
		case 'daniel':
			if(parseInt($('#enigma_daniel_answer').val())!=5) {
				alert(_invalid_sentences() + "\n\nNNNNNNOOOOOOOOOO!!!!!!!!!");
				return false;
			}
		break;
		case 'rogerio':
			if(parseInt($('#enigma_rogerio_answer').val())!=5) {
				alert(_invalid_sentences() + "\n\nNNNNNNOOOOOOOOOO!!!!!!!!!");
				return false;
			}
		break;
		case 'antonio':
			if(parseInt($('#enigma_antonio_answer').val())!=10) {
				alert(_invalid_sentences() + "\n\nNNNNNNOOOOOOOOOO!!!!!!!!!");
				return false;
			}
		break;
	}

	if (belongs_to_team(window.team, name)) {
		alert('BELONGS TO ANOTHER TEAM !!! TO LATE !!!!');
		$('.modal').modal('hide');
		return false;
	}




	switch(name) {
		case 'ana':
			$('#unlock-' + name + ' .modal-body').html('CHARACTER UNLOCKED :-). <br /><br/> Please find Ana in Café da Bica');
			$('#cross-ana, #cross-vera').css('display','block');
			$('#map-ana, #map-vera').hide();
		break;
		case 'vera':
			$('#unlock-' + name + ' .modal-body').html('CHARACTER UNLOCKED :-). <br /><br/>Please find Vera in Café da Fábrica');			
			$('#cross-ana, #cross-vera').css('display','block');
			$('#map-ana, #map-vera').hide();
		break;
		case 'liliana':
			$('#unlock-' + name + ' .modal-body').html('CHARACTER UNLOCKED :-). <br /><br/>Please find Liliana in Centro Comercial Amoreiras. Área de refeição. <br /><br/> Devem sair deste ponto por volta das 12:45:12');			
			$('#cross-liliana, #cross-daniel').css('display','block');
			$('#map-liliana, #map-daniel').hide();
		break;
		case 'daniel':
			$('#unlock-' + name + ' .modal-body').html('CHARACTER UNLOCKED :-). <br /><br/>Please find Daniel in Centro Comercial Colombo. Área de refeição. <br /><br/> Devem sair deste ponto por volta das 12:46:23');			
			$('#cross-liliana, #cross-daniel').css('display','block');
			$('#map-liliana, #map-daniel').hide();
		break;
		case 'rogerio':
			$('#unlock-' + name + ' .modal-body').html('CHARACTER UNLOCKED :-). <br /><br/>Please find Rogério. These are the coordinates N 38º 52´26.02´´ W 9º 16´ 41.45´´ \n\n Rua das Tomadas Nº10 2715-352 Negrais Portugal \n\n\ Campo Aberto');			
			$('#cross-rogerio, #cross-antonio').css('display','block');
			$('#map-rogerio, #map-antonio').hide();
		break;
		case 'antonio':
			$('#unlock-' + name + ' .modal-body').html('CHARACTER UNLOCKED :-). <br /><br/>Please find António.These are the coordinates N 38º 52´26.02´´ W 9º 16´ 41.45´´ \n\n Rua das Tomadas Nº10 2715-352 Negrais Portugal \n\n\ Campo Aberto');			
			$('#cross-rogerio, #cross-antonio').css('display','block');
			$('#map-rogerio, #map-antonio').hide();
		break;
	//}
	}


	$('#unlock-' + name + ' .modal-footer').hide();
	
	$.post( "/index/unlock_person", {team: window.team, name: name } );



	
}


var belongs_to_team  = function(team, name) {

	$.getJSON( "/index/person.json", {name : name}, function( data ) {
		var _obj = data[0];

		console.log(_obj.team + ' - ' + team);

		if(_obj.team != team && _obj.status == true) {
			alert('BELONGS TO ANOTHER TEAM !!! TO LATE !!!!'); 
		} 
		else if(_obj.team == team && _obj.status == true) {			
			alert('OK. Find ' + name + '!!!! \n\n Go to the next Panel');			
		}
		else {
			console.log('na tem cenas #unlock-' + name);
			$('#unlock-' + name).modal('show');	
		}		

	});
}




var _invalid_sentences = function(){

	var phrases = new Array();
	phrases[0] = "Hello, baby. Did you miss me?";
	phrases[1] = "This puny human will be no problem. I'll crush him in one blow.";
	phrases[2] = "You can look into my soul....but you don't own it.";
	phrases[3] = "Hmm...Thank God I didn't ask him to park the car.....";
	phrases[4] = "Eh eh. I don't think so.";
	phrases[5] = "Nice Dress!";
	phrases[6] = "This puny human will be no problem. I'll crush him in one blow.";
	phrases[7] = "Face your worst fear.";
	phrases[8] = "Fatality!!!";
	phrases[9] = "Thank God I didn't ask him to park the car.....";

	return phrases[parseInt(Math.floor(Math.random() * 9)) + 1];


}
