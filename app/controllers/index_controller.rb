class IndexController < ApplicationController


	#rails g migration boards 
	#rake db:drop db:create db:migrate

	def index

	end

	def unlock_new_board
		Boards.where("team = ? AND board = ?",params[:team].to_s,params[:board].to_i).update_all(:status => true)
		#render nothing: true
	end

	def unlock_board
		Boards.where("team = ? AND board = ?",params[:team].to_s,params[:board].to_i).update_all(:status => true)
		#Boards.update(params[:team].to_s,params[:board].to_i)
		render nothing: true
	end

	def unlock_person
		Team.where("name = ?",params[:name].to_s).update_all(:team => params[:team].to_s, :status => true)
		#Boards.update(params[:team].to_s,params[:board].to_i)
		render nothing: true
	end

	def boards
		respond_to do |format|			
			format.json  { render :json => Boards.get_all_boards().to_json } 
		end
	end

	def person
		respond_to do |format|			
			format.json  { render :json => Team.where("name = ?",params[:name]).to_json } 
		end
	end

	def persons
		respond_to do |format|			
			format.json  { render :json => Team.all.to_json } 
		end
	end
	
end