Rails.application.routes.draw do
  
  root to: 'index#index'

  get '/' => 'index#index'

  get '/index/boards' => 'index#boards'
  get '/index/person' => 'index#person'
  get '/index/persons' => 'index#persons'
  
  post '/index/unlock_board' => 'index#unlock_board'
  post '/index/unlock_person' => 'index#unlock_person'

  get '/unlock/:team/:board' => 'index#unlock_new_board'

end