class Boards < ActiveRecord::Migration

  def change  

  	create_table :boards do |t|  
      t.string :team  
      t.integer :board  
      t.boolean :status  
      t.timestamps  
    end    

    execute("insert into boards(team,board,status) values('diogo',0,true);")
    execute("insert into boards(team,board,status) values('diogo',1,true);")
    execute("insert into boards(team,board,status) values('diogo',2,false);")
    execute("insert into boards(team,board,status) values('diogo',3,false);")
    execute("insert into boards(team,board,status) values('diogo',4,false);")


    execute("insert into boards(team,board,status) values('jose',0,true);")
    execute("insert into boards(team,board,status) values('jose',1,true);")
    execute("insert into boards(team,board,status) values('jose',2,false);")
    execute("insert into boards(team,board,status) values('jose',3,false);")
    execute("insert into boards(team,board,status) values('jose',4,false);")

  end
  
end
